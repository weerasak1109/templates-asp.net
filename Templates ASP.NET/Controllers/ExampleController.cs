﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Templates_ASP.NET.Management;
using Templates_ASP.NET.Models;

namespace Templates_ASP.NET.Controllers
{
    public class ExampleController : ApiController
    {
        /// <summary>
        /// Example user info
        /// </summary>
        /// <param name="user_id">รหัสผู้ใช้</param>
        /// <param name="lang_id">รหัสภาษา</param>
        /// <returns></returns>
        [Route("user/info/{user_id}/{company_id}/{lang_id}")]
        public UserInfoModelsOut GetUserInfo(Int64 user_id, Int64 company_id, Int64 lang_id)
        {
            try
            {
                return ExampleManagement.I.UserInfo(user_id, company_id, lang_id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}