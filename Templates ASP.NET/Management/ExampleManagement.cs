﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Templates_ASP.NET.DBConnector;
using Templates_ASP.NET.Models;
using Templates_ASP.NET.Services;
using Templates_ASP.NET.Utilitys;

namespace Templates_ASP.NET.Management
{
    public class ExampleManagement
    {

        #region Singleton
        private ExampleService _service;
        private static readonly Lazy<ExampleManagement> instance = new Lazy<ExampleManagement>(() => Activator.CreateInstance<ExampleManagement>());
        public static ExampleManagement I
        {
            get
            {
                return instance.Value;
            }
        }
        public ExampleManagement()
        {
        }
        #endregion

        /// <summary>
        /// Example user info
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="company_id"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public UserInfoModelsOut UserInfo(Int64 user_id, Int64 company_id, Int64 lang_id)
        {
            UserInfoModelsOut res = null;

            try
            {
                IMongoDatabase conn = MongoConnector.I().DBConnect;
                _service = new ExampleService(conn);
                res = _service.UserInfo(user_id, company_id, lang_id);
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null) { ex = ex.InnerException; }
                if (_service != null)
                {
                    OutputBaseModel error = _service.GetException(ErrorCode.INTERNAL_ERROR, ErrorCode.INTERNAL_ERROR, lang_id, ex.Message);
                    res = new UserInfoModelsOut()
                    {
                        ErrorCode = error.ErrorCode,
                        ErrorSubCode = error.ErrorSubCode,
                        ErrorDetail = error.ErrorDetail,
                        ErrorMesg = error.ErrorMesg,
                        FlowControl = error.FlowControl,
                        Title = error.Title
                    };
                }
                else
                {
                    res = new UserInfoModelsOut()
                    {
                        ErrorCode = 1,
                        ErrorSubCode = 1,
                        ErrorDetail = ex.Message,
                        ErrorMesg = ex.Message,
                        FlowControl = 0,
                        Title = "Error"
                    };
                }
            }
            return res;
        }

    }
}