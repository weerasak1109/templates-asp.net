﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Templates_ASP.NET.Models;
using Templates_ASP.NET.Models.MongoDataBase;
using Templates_ASP.NET.Utilitys;

namespace Templates_ASP.NET.Services
{
    public class MongoSystemService
    {
        /// <summary>
        /// เชื่อมต่อฐานข้อมูล
        /// </summary>
        public IMongoDatabase _mongodb { get; set; }

        /// <summary>
        /// เรียก sequence name
        /// </summary>
        /// <param name="sequence_name">sequence name</param>
        public Int64 GetIncrement(string sequence_name)
        {
            try
            {

                var colle = _mongodb.GetCollection<counters>(typeof(counters).Name);
                counters auto_inc = new counters();
                List<counters> cou = colle.Find(u => u.t_name == sequence_name).ToList();
                if (cou.Count == 1)
                {
                    counters info_counters = cou.First();
                    info_counters.seq += 1;
                    var query = Builders<counters>.Filter.Eq(f => f.t_name, sequence_name);
                    var update = Builders<counters>.Update.Set(o => o.seq, info_counters.seq);
                    var opts = new FindOneAndUpdateOptions<counters>()
                    {
                        ReturnDocument = ReturnDocument.After
                    };
                    auto_inc = colle.FindOneAndUpdate(query, update, opts);
                }
                else
                {
                    auto_inc = new counters()
                    {
                        seq = 1,
                        t_name = sequence_name
                    };
                    colle.InsertOne(auto_inc);
                }
                if (auto_inc == null) throw new Exception("Not Increment");
                return auto_inc.seq;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="error_code"></param>
        /// <param name="sub_error_code"></param>
        /// <param name="lang_id"></param>
        /// <param name="plus_mesg"></param>
        /// <returns></returns>
        public OutputBaseModel GetException(int error_code, int sub_error_code, Int64 lang_id, string plus_mesg)
        {
            OutputBaseModel output = new OutputBaseModel()
            {
                ErrorCode = error_code,
                ErrorSubCode = sub_error_code
            };
            try
            {
                #region Collection
                IMongoCollection<zz_error_message> colle = this._mongodb.GetCollection<zz_error_message>(typeof(zz_error_message).Name);
                #endregion

                #region Query
                List<zz_error_message> query = null;
                zz_error_message error_message = null;
                query = colle.Find(i => i.err_id == error_code && i.err_sub == sub_error_code).ToList();
                #endregion

                #region Output
                if (query.Count > 0)
                {
                    error_message = query.First();
                    if (lang_id == Language.TH)
                    {
                        output.Title = error_message.title2;
                        output.ErrorMesg = error_message.err_msg2;
                        output.ErrorDetail = string.Format("{0} {1}", error_message.err_fix2, plus_mesg);
                    }
                    else if (lang_id == Language.EN)
                    {
                        output.Title = error_message.title1;
                        output.ErrorMesg = error_message.err_msg1;
                        output.ErrorDetail = string.Format("{0} {1}", error_message.err_fix1, plus_mesg);
                    }
                }
                else
                {
                    query = colle.Find(i => i.err_id == 100 && i.err_sub == -1).ToList();
                    if (query.Count > 0)
                    {
                        error_message = query.First();
                        output.Title = error_message.title1;
                        output.ErrorMesg = error_message.err_msg2;
                        output.ErrorDetail = string.Format("{0} {1}", error_message.err_fix2, plus_mesg);
                    }
                    else
                    {
                        output.Title = "ความผิดพลาด";
                        output.ErrorMesg = "ไม่สามารถระบุความผิดพลาดได้";
                        output.ErrorDetail = string.Format("{0} {1}", "ไม่สามารถระบุความผิดพลาดได้ กรุณาติดต่อผู้ดูแลระบบของท่าน", plus_mesg);
                    }
                }
                return output;
                #endregion
            }
            catch (Exception exce)
            {
                throw exce;
            }
        }
    }
}