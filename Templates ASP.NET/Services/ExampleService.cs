﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using Templates_ASP.NET.Models;
using Templates_ASP.NET.Utilitys;
using static Templates_ASP.NET.Models.MongoDataBase.DataBaseMasterEntity;

namespace Templates_ASP.NET.Services
{
    public class ExampleService : MongoSystemService
    {
        /// <summary>
        ///  logic constructor
        /// </summary>
        /// <param name="mongodb"></param>
        public ExampleService(IMongoDatabase conn)
        {
            this._mongodb = conn;
        }

        /// <summary>
        /// Example user info
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="lang_id"></param>
        /// <returns></returns>
        public UserInfoModelsOut UserInfo(Int64 user_id, Int64 company_id, Int64 lang_id)
        {
            try
            {
                var z_user = this._mongodb.GetCollection<z_user>(typeof(z_user).Name);
                UserInfoModelsOut output_model = new UserInfoModelsOut();
                if (user_id > 0 && company_id > 0)
                {
                    List<z_user> info_user = z_user.Find(t => t.user_id == user_id && t.inactive == false && t.locked == false && t.activated == true && t.company_id == company_id).ToList();
                    if (info_user.Count > 0)
                    {
                        z_user info = info_user.First();
                        output_model.Data = new UserInfoModels()
                        {
                            addr = info.addr,
                            date_of_birth = info.date_of_birth,
                            email = info.email,
                            f_name = info.f_name,
                            id_card = info.id_card,
                            image = info.image,
                            l_name = info.l_name,
                            mobile = info.mobile,
                            username = info.username,
                            user_id = info.user_id,
                            user_type = info.user_type,
                            status_online = new status_online()
                            {
                                is_online = info.online.is_online,
                                offline_datetime = info.online.offline_datetime,
                                online_datetime = info.online.online_datetime
                            }
                        };

                        output_model.ErrorCode = ErrorCode.SUCCESS;
                        output_model.ErrorSubCode = ErrorCode.SUCCESS;
                    }
                    else
                    { // หา user ไม่เจอ
                        OutputBaseModel err = this.GetException(ErrorCode.INTERNAL_ERROR, ErrorCode.INTERNAL_ERROR, lang_id, "");
                        output_model.ErrorCode = err.ErrorCode;
                        output_model.ErrorSubCode = err.ErrorSubCode;
                        output_model.Title = err.Title;
                        output_model.ErrorMesg = err.ErrorMesg;
                        output_model.ErrorDetail = err.ErrorDetail;
                        return output_model;
                    }
                }
                else
                { // ระบุบ user_id ไม่ถูก
                    OutputBaseModel err = this.GetException(ErrorCode.INTERNAL_ERROR, ErrorCode.INTERNAL_ERROR, lang_id, "");
                    output_model.ErrorCode = err.ErrorCode;
                    output_model.ErrorSubCode = err.ErrorSubCode;
                    output_model.Title = err.Title;
                    output_model.ErrorMesg = err.ErrorMesg;
                    output_model.ErrorDetail = err.ErrorDetail;
                    return output_model;
                }
                return output_model;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
              ex);
            }
        }

    }
}