﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Templates_ASP.NET.Utilitys
{
    public class Language
    {
        public const int EN = 1;
        public const int TH = 2;
        public const int OTHER = 3;
    }

    public class ErrorCode
    {
        public const int SUCCESS = 1;
        public const int UNKNOWN = 0;

        public const int INTERNAL_ERROR = -1000;
    }
}