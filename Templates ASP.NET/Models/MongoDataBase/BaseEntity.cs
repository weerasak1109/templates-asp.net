﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Templates_ASP.NET.Models.MongoDataBase
{
    /// <summary>
    /// ข้อมูลพื้นฐาน
    /// </summary>
    public abstract class DataBaseEntity
    {
        /// <summary>
        /// รหัส
        /// </summary>
        public ObjectId _id { get; set; }
    }

    /// <summary>
    /// ข้อมูลคงที่
    /// </summary>
    public abstract class DataConstEntity : DataBaseEntity
    {
        /// <summary>
        /// ไม่ใช้งาน
        /// </summary>
        public Boolean inactive { get; set; }
        /// <summary>
        /// ผู้สร้าง
        /// </summary>
        public Int64 createAt { get; set; }
        /// <summary>
        /// เวลาสร้าง
        /// </summary>
        public DateTime create_date { get; set; }
        /// <summary>
        /// สร้าง
        /// </summary>
        /// <param name="user_id">ผุ้สร้าง</param>
        public virtual void Create(Int64 user_id)
        {
            this.createAt = user_id;
            this.create_date = DateTime.Now;
            this.inactive = false;
        }
    }

    /// <summary>
    /// ข้อมูลตั้งค่า
    /// </summary>
    public abstract class DataSettingEntity : DataConstEntity
    {
        /// <summary>
        /// ผู้แก้ไข
        /// </summary>
        public Int64 editAt { get; set; }
        /// <summary>
        /// เวลาแก้ไข
        /// </summary>
        public DateTime edit_date { get; set; }
        /// <summary>
        /// แก้ไข
        /// </summary>
        /// <param name="user_id">ผู้แก้ไข</param>
        public virtual void Edit(Int64 user_id)
        {
            this.editAt = user_id;
            this.edit_date = DateTime.Now;
        }
    }

    /// <summary>
    /// ข้อมูลหลัก
    /// </summary>
    public abstract class DataMasterEntity : DataSettingEntity
    {
        /// <summary>
        /// รหัสบริษัท
        /// </summary>
        public Int64 company_id { get; set; }
    }
}