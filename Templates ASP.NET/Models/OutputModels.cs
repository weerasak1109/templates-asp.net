﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Templates_ASP.NET.Models
{
    /// <summary>
    /// ข้อมูลส่งออก
    /// </summary>
    public class OutputBaseModel
    {
        /// <summary>
        /// รหัสผิดพลาด
        /// </summary>
        [Required]
        public int ErrorCode { get; set; }
        /// <summary>
        /// รหัสย่อยผิดพลาด
        /// </summary>
        [Required]
        public int ErrorSubCode { get; set; }
        /// <summary>
        /// หัวผิดพลาด
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// ข้อความผิดพลาด
        /// </summary>
        public string ErrorMesg { get; set; }
        /// <summary>
        /// รายละเอียดผิดพลาด
        /// </summary>
        public string ErrorDetail { get; set; }
        /// <summary>
        /// ควบคุมการไหล
        /// </summary>
        [Required]
        public int FlowControl { get; set; }
    }
}