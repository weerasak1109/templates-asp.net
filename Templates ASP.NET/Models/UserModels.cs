﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Templates_ASP.NET.Models.MongoDataBase.DataBaseMasterEntity;

namespace Templates_ASP.NET.Models
{
    public class UserInfoModelsOut : OutputBaseModel
    {
        public UserInfoModels Data { get; set; }
    }

    public class UserInfoModels
    {
        /// <summary>
        /// รหัสผู้ใช้
        /// </summary>
        public Int64 user_id { get; set; }
        /// <summary>
        /// ประเภทผู้ใช้
        /// </summary>
        public Int64 user_type { get; set; }
        /// <summary>
        /// ยูสเซ่อเนม
        /// </summary>
        public string username { get; set; }
        /// <summary>
        /// ชื่อ
        /// </summary>
        public string f_name { get; set; }
        /// <summary>
        /// นามสกุล
        /// </summary>
        public string l_name { get; set; }
        /// <summary>
        /// เบอร์มือถือ
        /// </summary>
        public string mobile { get; set; }
        /// <summary>
        /// อีเมล
        /// </summary>
        public string email { get; set; }
        /// <summary>
        /// เลขประจำตัวประชาชน
        /// </summary>
        public string id_card { get; set; }
        /// <summary>
        /// วันเกิด
        /// </summary>
        public DateTime? date_of_birth { get; set; }
        /// <summary>
        /// บ้านเลขที่ หมู่ หมู่บ้าน
        /// </summary>
        public string addr { get; set; }
        /// <summary>
        /// รูปผู้ใช้งาน
        /// </summary>
        public img image { get; set; }
        /// <summary>
        /// สถานะการใช้งาน
        /// </summary>
        public status_online status_online { get; set; }
    }
}